module Api
  module V1
    class SchemasController < ApplicationController
      before_action :load_schema, only: [:show, :update, :destroy]

      # GET /api/v1/schemas
      def index
        @schemas = Schema.all

        respond_to do |format|
          format.json { render }
        end
      end

      # GET /api/v1/schemas/3
      def show
        respond_to do |format|
          format.json { render }
        end
      end

      # POST /api/v1/schemas
      def create
        @schema = Schema.new(schema_params)

        respond_to do |format|
          if @schema.save
            format.json {render action: :show, status: :created}
          else
            format.json {render json: {errors: @schema.errors}, status: :unprocessable_entity}
          end
        end
      end

      # PUT /api/v1/schemas/3
      def update
        respond_to do |format|
          if @schema.update_attributes(schema_params)
            format.json { head :no_content }
          else
            format.json { render json: @schema.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /api/v1/schemas/3
      def destroy
        @schema.destroy

        respond_to do |format|
          format.json { head :no_content }
        end
      end

      private

      def load_schema
        @schema = Schema.find(params[:id])
      end

      def schema_params
        params.require(:schema).permit(:name, :user_id, :elements)
      end
    end
  end
end