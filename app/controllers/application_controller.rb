class ApplicationController < ActionController::Base
  rescue_from JSON::ParserError, with: :bad_json

  # protect_from_forgery with: :exception
  protect_from_forgery with: :null_session, only: Proc.new { |c| c.request.format.json? }

  private
  def bad_json(exception)
    render json: {error: exception}, status: :unprocessable_entity
  end
end
