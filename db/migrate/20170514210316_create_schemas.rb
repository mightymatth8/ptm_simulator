class CreateSchemas < ActiveRecord::Migration[5.0]
  def change
    create_table :schemas do |t|
      t.belongs_to :user, index: true
      t.string :name
      t.text :elements
      t.timestamps
    end
  end
end
