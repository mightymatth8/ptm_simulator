# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# rails db:drop && rails db:create && rails db:migrate && rails db:seed

puts 'Creating a user...'
user = User.create(email: 'somebody@email.com',
                   password: '123123123')

puts 'Creating a schema...'

elements = {
    nodes: [
        {
            name: 'v1',
            reliability: 0.85,
            neighbours: ['v2', 'v3']
        },
        {
            name: 'v2',
            reliability: 0.85,
            neighbours: ['v1', 'v3', 'v4']
        },
        {
            name: 'v3',
            reliability: 0.85,
            neighbours: ['v1', 'v2', 'v4']
        },
        {
            name: 'v4',
            reliability: 0.85,
            neighbours: ['v2', 'v3']
        }
    ],
    links: [
        {
            name: 'e1',
            reliability: 0.85,
            nodes: ['v1', 'v2']

        },
        {
            name: 'e2',
            reliability: 0.85,
            nodes: ['v2', 'v4']
        },
        {
            name: 'e3',
            reliability: 0.85,
            nodes: ['v1', 'v3']
        },
        {
            name: 'e4',
            reliability: 0.85,
            nodes: ['v3', 'v4']
        },
        {
            name: 'e5',
            reliability: 0.85,
            nodes: ['v2', 'v3']
        }],
}


Schema.create(user_id: user.id,
              name: 'First Schema',
              elements: JSON.parse(elements.to_json))
